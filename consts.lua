consts = { 
	centerX = display.contentCenterX,
	centerY = display.contentCenterY,
	width = display.contentWidth,
	height = display.contentHeight,
	mmBtnHeight = 100,
	gmBtnHeight = 60,
	gmBtnWidth = 280,
	playerR = 43,
	puckR = 25,
	tableWidth = 460,
	tableHeight = 740
}

return consts