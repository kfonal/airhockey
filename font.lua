local device = require "device"

local font = {}
font.normal = "venusris"
font.roboto = "Roboto-Bold"

if ( device.isSimulator ) then
   font.normal = "Venus Rising"
   font.roboto = "Roboto"
end

return font