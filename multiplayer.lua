local social = require "social"
local storyboard = require "storyboard"
local json = require "json"

local multiplayer = { 
	--members
	playerType = "", --guest, host
	onMessageReceived = nil,
	roomId = nil,
	otherPlayerId = nil,
	--functions
	selectPlayer = nil, 
	invitations = nil, 
	sendMessage = nil,
	createMessage = nil
}

local messagesReceived = 0

local function messageListener(event)
	print("Received message("..messagesReceived.."): "..event.data.message)
	messagesReceived = messagesReceived + 1

	local message = json.decode( event.data.message )

	if (message.type == "testEnded") then
		--if more then 20 messages was received then connection is enough good
		if (messagesReceived > 20) then
			storyboard.gotoScene( "gamescene", { effect = "fromTop", time = 300, params = { mode = "multiplayer", playerType = multiplayer.playerType } } )
		else
			--TODO leave room - connection too slow
		end
	elseif (message.type ~= "test") then
		if (multiplayer.onMessageReceived) then
			multiplayer.onMessageReceived(message)
		end
	end
end

local function enterRoom(event)
	if (event.data.phase == "start") then
		--set other playerID and roomID
		multiplayer.otherPlayerId = event.data[1]
		multiplayer.roomId = event.data.roomID

		--everything was properly connected so set message listeners
		social.request( "setMessageReceivedListener", { listener = messageListener } )

		--test connection, if host then through 2 sec send messages
		print "start testing connection"
		
		local startTime = system.getTimer()
		local function testConnection()
		  	if (system.getTimer() - startTime < 2000) then
		    	print("message: "..(system.getTimer() - startTime))
		    	local message = multiplayer.createMessage("test", "testMessage")
				multiplayer.sendMessage(message)
		    	timer.performWithDelay( 20, testConnection )
		  	else
			  	--send message about end testing, after this game will start
				multiplayer.sendMessage(multiplayer.createMessage("testEnded", "test ended"))
			end
		end
		testConnection()

		print "end testing connection"
	end
	--TODO react if phase == cancel
end

local function roomListener(event)
	if (event.type == "joinRoom" or event.type == "createRoom") then
		if event.data.isError then 
			print ("error by "..event.type)
			for k,v in pairs( event.data ) do
				print( tostring(k)..":"..tostring(v) )
			end
		else
			social.showWaitingRoom( {
				listener = enterRoom,
				roomID = event.data.roomID,
				minPlayers = 0
			} )
		end
	end
end

multiplayer.selectPlayer = function()
	local function selectPlayersListener(selectPlayerEvent)
		-- Create a room with only the first selection
		print "Creating room for first selection"
		multiplayer.playerType = "host"
		local playerId = { selectPlayerEvent.data[1] }
		social.request("createRoom", {
			listener = roomListener,
			playerIDs = playerId
		})
	end
	
	--leave room if currently active
	multiplayer.leaveRoom()

	--show player selection view
	social.showSelectPlayers( { 
		listener = selectPlayersListener,
		minPlayers = 1,
		maxPlayers = 1
	} )
end

multiplayer.invitations = function()
	local function invitationsListener(invitationsEvent)
		print( "joining room: "..invitationsEvent.data.roomID )
		multiplayer.playerType = "guest"
		social.request("joinRoom",
		{
			roomID = invitationsEvent.data.roomID,
			listener = roomListener
		})
	end

	social.showInvitations({
		listener = invitationsListener
	})
end

--[[
type of messages:
    -test  
    -testEnded
    -move
]]
multiplayer.createMessage = function(type, content)
	local message = { type = type, content = content }
	return message
end

multiplayer.sendMessage = function(message)
	social.request("sendMessage", 
	{
		playerIDs = {multiplayer.otherPlayerId},
		roomID = multiplayer.roomId,
		message = json.encode( message ),
		reliable  = false,
	})
end

multiplayer.leaveRoom = function()
	if (multiplayer.roomId) then
		social.request("leaveRoom", 
			{
				roomID = storyboard.matchId,
			})
	end
end

return multiplayer