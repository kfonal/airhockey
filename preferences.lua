local str = require "str"
local json = require "json"
local filePath = system.pathForFile( "preferences.txt", system.DocumentsDirectory )

local Preferences = { theme = "ice", goalsToWin = 5, cpuLevel = "easy", fbLogin = false, fbTokenExpiration = "" }

function Preferences:load()
	local file = io.open( filePath, "r" )

	if file then
		local fileContent = file:read("*a")

		--support for old version of file
		if (string.find(fileContent, "=")) then
			local fileData = str.split(fileContent, ",")

			print("Preferences: ")
			for i = 1, #fileData do
				local keyValue = str.split(fileData[i], "=")
				if (keyValue[1] == "goalsToWin") then
					self[keyValue[1]] = tonumber(keyValue[2])
				else
					self[keyValue[1]] = keyValue[2]
				end
				print(keyValue[1].."="..tostring(keyValue[2]))
			end
		else
			local jsonData = json.decode( fileContent )

			print("Preferences: ")
			for k,v in pairs(jsonData) do
				self[k] = v
				print(k.."="..tostring(v))
			end
		end

		io.close(file)
	else
		print("Preferences file doesn't exist")	
	end
end

function Preferences:save()
	local file = io.open( filePath, "w" )

	local prefData = {}
	for k,v in pairs(self) do
		if (type(v) ~= "function") then
			prefData[k] = v
		end
	end

	file:write(json.encode(prefData))
	io.close(file)
end

function Preferences:getThemeTextColor()
	if (self.theme == "wood") then return 1, 0.7, 0.2
	else return 0,0,1 end
end

return Preferences
