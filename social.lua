local json = require "json"
local facebook = require "facebook"
local preferences = require "preferences"
local gameNetwork = require "gameNetwork"

local social = {}

social.achievements = { 
    easyCpuSlayer = "CgkIjfL9vZ4FEAIQBg", 
    mediumCpuSlayer = "CgkIjfL9vZ4FEAIQBw", 
    hardCpuSlayer = "CgkIjfL9vZ4FEAIQCA",
    streakWinner = "STREAK WINNER",
    airHockeyMaster = "CgkIjfL9vZ4FEAIQCg" }

social.fbOnLoginEvent = nil
social.fbOnLogoutEvent = nil

social.init = function()
    gameNetwork.init( "google", 
        function()
            gameNetwork.request( "login", { userInitiated = false } )
        end 
    )
end

social.isConnected = function()
    return gameNetwork.request( "isConnected" )
end

social.login = function(loginListener)
    gameNetwork.request("login",
        {
            listener = loginListener,
            userInitiated = true
        })
end

social.logout = function()
    gameNetwork.request( "logout" )
end

social.unlockAchievement = function(achievement) 
    gameNetwork.request( "unlockAchievement", { achievement = { identifier = achievement } } )
end

social.showAchievements = function()
    print "show achievements"
    gameNetwork.show( "achievements" )
end

social.showLeaderboards = function()
    print "show leaderboards"
    gameNetwork.show( "leaderboards" )
end

social.showWaitingRoom = function(params)
    print "show waiting room"
    gameNetwork.show("waitingRoom", params)
end

social.showSelectPlayers = function(params)
    print "show select players"
    gameNetwork.show( "selectPlayers", params )
end

social.showInvitations = function(params)
    print "show invitations"
    gameNetwork.show( "invitations", params )
end

social.request = function(request, params)
    print( "call request: "..request )
    gameNetwork.request( request, params )
end

social.fbListener = function(event)
	--log debug info
	print( "event.name", event.name )  --"fbconnect"
    print( "event.type:", event.type ) --type is either "session", "request", or "dialog"
    print( "isError: " .. tostring( event.isError ) )
    print( "didComplete: " .. tostring( event.didComplete ) )

    if ( "session" == event.type ) then
        --options are: "login", "loginFailed", "loginCancelled", or "logout"
        if ( "login" == event.phase ) then
            print("facebook login successful. Token validity: "..event.expiration.."(now: "..os.time()..")")
            preferences.fbLogin = true
            preferences.fbTokenExpiration = event.expiration
            preferences:save()
            if (social.fbOnLoginEvent) then
            	social.fbOnLoginEvent(event)
            end
            --code for tasks following a successful login
        elseif ( "logout" == event.phase ) then
            print("FB logout successfully")
            preferences.fbLogin = false
            preferences.fbTokenExpiration = false
            preferences:save()
            if (social.fbOnLogoutEvent) then
                social.fbOnLogoutEvent(event)
            end
        end

    elseif ( "request" == event.type ) then
        print("facebook request")
        if ( not event.isError ) then
            local response = json.decode( event.response )
            --process response data here
        end

    elseif ( "dialog" == event.type ) then
        print( "dialog", event.response )
        --handle dialog results here
    end
end

social.fbConnect = function(onLoginEvent)
	social.fbOnLoginEvent = onLoginEvent
	facebook.login( "1459137444316975", social.fbListener, { "publish_actions" } )
end

social.fbLogout = function(onLogoutEvent)
    social.fbOnLogoutEvent = onLogoutEvent
    facebook.logout()
end

--this function verify if token is still validatd and if it is then login to fb automatically
social.fbSingleSignOn = function(onLoginEvent)
    if (not preferences.fbLogin) then
        print("Not logged to FB")
        return
    elseif (os.time() >= preferences.fbTokenExpiration) then
        print("FB token expired")
        preferences.fbLogin = false
        return
    end

    local function networkListener( event )
        if ( event.isError ) then
            print( "Network error! No possible to connect to FB")
        else
            --network is fine - try to connect to FB
            print("auto-connect to FB")
            social.fbConnect(onLoginEvent)
        end
    end

    -- Access Google over SSL to verify connection
    network.request( "https://encrypted.google.com", "GET", networkListener )
end

return social