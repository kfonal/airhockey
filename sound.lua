local audio = require "audio"

local Sounds = { }

Sounds.goalSound = audio.loadSound("goal.wav")
Sounds.collisionPuckPlayerSound = audio.loadSound("puck_player.wav")
Sounds.collisionPuckTableSound = audio.loadSound("puck_table.wav")

function Sounds:playGoalSound()
	audio.play(self.goalSound)
end

function Sounds:playCollisionPuckPlayerSound()
	audio.play(self.collisionPuckPlayerSound)
end

function Sounds:playCollisionPuckTableSound()
	audio.play(self.collisionPuckTableSound, { duration = 250 })
end

return Sounds;