display.setStatusBar(display.HiddenStatusBar)

local storyboard = require "storyboard"
local consts = require "consts"
local preferences = require "preferences"
local ads = require "ads"
local cb = require "chartboostSDK.chartboost"
local social = require "social"

--android hardware keys support
local function onKeyEvent( event )
	local phase = event.phase
   	local keyName = event.keyName
   
   	if ( "back" == keyName and phase == "up" ) then
    	if ( storyboard.currentScene == "splash" ) then
         	native.requestExit()
      	else
        	if ( storyboard.isOverlay and not storyboard.overlayBlocked) then
         		storyboard.hideOverlay( "zoomOutInFade", 300 )
         	elseif ( storyboard.onBackEvent ) then
         		storyboard.onBackEvent()
        	elseif ( storyboard.backTo ) then
        		storyboard.gotoScene( storyboard.backTo , { effect = "fromTop", time = 300 } )
        	else
            	native.requestExit()
      		end
      	end
    end

    if ( keyName == "volumeUp" and phase == "down" ) then
		local masterVolume = audio.getVolume()
	  	print( "volume:", masterVolume )
	  
	  	if ( masterVolume < 1.0 ) then
	     	masterVolume = masterVolume + 0.1
	    	audio.setVolume( masterVolume )
	    end

	  	return true
    elseif ( keyName == "volumeDown" and phase == "down" ) then
      	local masterVolume = audio.getVolume()
      
      	print( "volume:", masterVolume )
      	if ( masterVolume > 0.0 ) then
        	masterVolume = masterVolume - 0.1
        	audio.setVolume( masterVolume )
      	end
   
      	return true
   	end

   return true
end

--show splash screen
local splash = display.newImageRect( "splashscreen.png", consts.width, consts.height )
splash.x = consts.centerX
splash.y = consts.centerY

--add listeners
Runtime:addEventListener( "key", onKeyEvent )

--init ad systems
ads.init( "admob", "a151bb9592cb18f", adListener )

--init social networks like google play services
social.init()

cb.create{ appId = "51be335e16ba475669000002", 
  appSignature = "ac4eaf0957590edd91ea14e556fcf820ad055874", 
  appDelagate = nil, 
  appBundle = "org.fonal.game.airHockey" 
}
cb.startSession()

--load preferences and activate multitouch
preferences:load()
system.activate( "multitouch" )

--goto main menu
timer.performWithDelay( 2000, function()
    splash:removeSelf()
    storyboard.gotoScene("mainmenu")
end )