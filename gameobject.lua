local consts = require "consts"
local physics = require "physics"
local sound = require "sound"
local cpustrategy = require "cpustrategy"
local multiplayer = require "multiplayer"

local CATEGORY_PLAYER = 0x0001
local CATEGORY_PUCK = 0x2000
local CATEGORY_GOAL_SENSOR = 0x0004
local CATEGORY_HALF_OF_TABLE1 = 0x0010
local CATEGORY_HALF_OF_TABLE2 = 0x0020
local CATEGORY_TABLE = 0x1000
local CATEGORY_ALL = 0xFFFF

local function dragBody( event )
	--if it's cpu player then don't touch
	if (event.target.owner.cpuStrategy) then return true end

    local body = event.target
    local phase = event.phase
    local stage = display.getCurrentStage()
 
    if ("began" == phase) then
    	stage:setFocus( body, event.id )
        body.isFocus = true
     elseif (body.isFocus) then
         if ("moved" == phase) then
       	 	-- Update the joint to track the touch
       	 	x, y = event.x, event.y + ((event.y < consts.centerY) and consts.playerR/2 or -consts.playerR/2)
            body.touchJoint:setTarget( x, y)

            --if multiplayer mode and guest then send move message
            if (body.owner.mode == "multiplayer" and multiplayer.playerType == "guest") then
            	multiplayer.sendMessage(multiplayer.createMessage("move", x..";"..y))
            end
        elseif ("ended" == phase or "cancelled" == phase) then
        	stage:setFocus( body, nil )
            body.isFocus = false
        end
     end
 
    -- Stop further propagation of touch event
    return true
end

local GameObject = { isTouchable = false, canUpdatePosition = true, body = nil, name = "" }

function GameObject:resetForces()
	if (self.body) then
		self.body:setLinearVelocity( 0, 0 )
		self.body.angularVelocity = 0
	end
end

function GameObject:setPosition(x,y)
	if (self.body) then
		self.body.x, self.body.y = x,y
		if (self.body.touchJoint) then
			self.body.touchJoint:setTarget(x, y)
		end
	end
end

function GameObject:getPosition()
	if (self.body) then
		return self.body.x, self.body.y
	else
		return -1, -1
	end
end

function GameObject:hide()
	if (self.body) then
		self.body.isVisible = false
	end
end

function GameObject:show()
	if (self.body) then
		self.body.isVisible = true
	end
end

function GameObject:new()
	local o = {}

	setmetatable( o, self )
	self.__index = self
	
	return o
end

function GameObject:newTable(parent, image)
	local table = self:new()

	table.name = "table"

	local topLeft = { -230,-370, -230,-400, -90,-400, -90,-370 } 
	local topRight = { 90,-370, 90,-400, 230,-400, 230,-370 }
	local topGoal = { -90,-400, 90,-400, 90,-390, -90,-390}
	local left = { -240,-370, -230,-370, -230,370, -240,370 }
	local bottomLeft = { -230,400, -230,370, -90,370, -90,400 } 
	local bottomRight = { 90,400, 90,370, 230,370, 230,400  }
	local bottomGoal = { -90,400, -90,390, 90,390, 90,400  }
	local right = { 230,-370, 240,-370, 240,370, 230,370 }
	local halfPlayer1 = { -230,-consts.playerR, 230,-consts.playerR, 230,-consts.playerR-1, -230,-consts.playerR-1 }
	local halfPlayer2 = { -230,consts.playerR, 230,consts.playerR, 230,consts.playerR+1, -230,consts.playerR+1 }

	table.body = display.newImageRect( image, consts.width, consts.height )
	table.body.x = consts.centerX
	table.body.y = consts.centerY
	parent:insert(table.body)

	physics.addBody( table.body, "static", 
		{ shape = topGoal, filter = { categoryBits = CATEGORY_TABLE } }, --goal for player1
		{ shape = bottomGoal, filter = { categoryBits = CATEGORY_TABLE } }, --goal for player2
		
		{ shape = left, filter = { categoryBits = CATEGORY_TABLE } },
		{ shape = right, filter = { categoryBits = CATEGORY_TABLE } },
		{ shape = bottomLeft, filter = { categoryBits = CATEGORY_TABLE } },
		{ shape = bottomRight, filter = { categoryBits = CATEGORY_TABLE } },
		{ shape = topLeft, filter = { categoryBits = CATEGORY_TABLE } },
		{ shape = topRight, filter = { categoryBits = CATEGORY_TABLE } },
		{ shape = halfPlayer1, filter = { categoryBits = CATEGORY_HALF_OF_TABLE1 } },
		{ shape = halfPlayer2, filter = { categoryBits = CATEGORY_HALF_OF_TABLE2 } } )
	table.body.owner = table
end

local function onPlayerPostCollision(self, event)
	if (self.owner.cpuStrategy and event.other.owner.name == "puck") then
		self.owner.cpuStrategy.timeOfLastAttack = 0.0 
	end
end

function GameObject:newPlayer(parent, name, image, x, y, cpuStrategy)
	local player = self:new()
	
	player.name = name
	player.isTouchable = true
	player.scores = 0

	player.body = display.newImageRect( image, consts.playerR*2, consts.playerR*2 )
	player.body.x = x
	player.body.y = y
	parent:insert(player.body)

	local maskBits = CATEGORY_TABLE + CATEGORY_PUCK + CATEGORY_PLAYER;
	if (y > consts.centerY) then maskBits = maskBits + CATEGORY_HALF_OF_TABLE1 else maskBits = maskBits + CATEGORY_HALF_OF_TABLE2 end
	physics.addBody( player.body, "dynamic", 
		{density=1.0, friction=0.4, bounce=0.0 , radius=consts.playerR , 
		filter = { categoryBits = CATEGORY_PLAYER, maskBits = maskBits }})
	player.body.isFixedRotation = true
	player.body.owner = player

	player.body:addEventListener( "touch", dragBody )

	player.body.touchJoint = physics.newJoint( "touch", player.body, x, y )
    player.body.touchJoint.dampingRatio = 0.4
    player.body.touchJoint.maxForce = 10000

    --if strategy was passed then it is cpu player
    if (cpuStrategy) then
    	player.body.postCollision = onPlayerPostCollision
    	--player.body:addEventListener( "postCollision", player.body )
    	if (cpuStrategy == "easy") then player.cpuStrategy = cpustrategy.easy
    	elseif (cpuStrategy == "medium") then player.cpuStrategy = cpustrategy.medium
    	elseif (cpuStrategy == "hard") then player.cpuStrategy = cpustrategy.hard 
    	end
    end

    return player
end

local function onPuckCollision(self, event)
	if (event.phase == "began") then
		--print("puck collide with "..event.other.owner.name.."el: "..event.otherElement)

		--if collision is with table then it is goal or cushion
		if (event.other.owner.name == "table") then
			sound:playCollisionPuckTableSound()

			if (event.otherElement <= 2) then 
				--collision with goal sensor - raise goal event
				self.owner.onGoal({ goalFor = "player"..event.otherElement })
				sound:playGoalSound()
			end
		else --other case it was collision with a player
			sound:playCollisionPuckPlayerSound()
		end
	end
end

local function onPuckPostCollision(self, event)
	if (event.other.owner.name == "table") then
		
		local vx, vy = self:getLinearVelocity()
		
		local minV = 10
		--left cushion
		if (event.otherElement == 3) then 
			print("left cushion")  
			if (vx < minV) then vx = minV; print("change vx: "..vx) end
		end
		--right cushion
		if (event.otherElement == 4) then  
			if (-vx < minV) then vx = -minV end
		end
		--bottom cushion
		if (event.otherElement == 5 or event.otherElement == 6) then 
			if (-vy < minV) then vy = -minV end
		end
		--top cushion
		if (event.otherElement == 7 or event.otherElement == 8) then 
			if (vy < minV) then vy = minV end
		end

		self:setLinearVelocity(vx, vy)
	end
end

function GameObject:newPuck(parent, image, x, y)
	local puck = self:new()
	
	puck.name = "puck"

	puck.body = display.newImageRect( image, consts.puckR*2, consts.puckR*2 )
	puck.body.x = x
	puck.body.y = y
	parent:insert(puck.body)

	physics.addBody( puck.body, "dynamic", 
		{density=1.0, friction=0.5, bounce=0.65 , radius=consts.puckR, 
		filter = { categoryBits = CATEGORY_PUCK, maskBits = CATEGORY_TABLE + CATEGORY_PLAYER }  } )
	puck.body.owner = puck
	puck.body.isFixedRotation = true
	puck.body.vxByCollision, puck.body.vyByCollision = 0, 0

	puck.body.collision = onPuckCollision
	puck.body.preCollision = onPuckPreCollision
	puck.body.postCollision = onPuckPostCollision
	puck.body:addEventListener( "collision", puck.body )
	puck.body:addEventListener( "postCollision", puck.body )

	return puck
end

return GameObject