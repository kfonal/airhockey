local consts = require "consts"
local storyboard = require "storyboard"
local preferences = require "preferences"
local widget = require "widget"
local font = require "font"

local scene = storyboard.newScene( )

--[[menu during game
buttons and their behaviour base on params passed to showOerlay method from storyboard
params should contain table - buttons = {} which contains pairs of {text, listener}
]]
function scene:createScene(event)
	local menu = self.view

	local bg = display.newRect( consts.centerX, consts.centerY, consts.width, consts.height )
	bg:setFillColor( 0, 0, 0, 0.6 )
	menu:insert( bg )

	local top = consts.centerY + 50 - 120 * #event.params.buttons / 2
	for k, button in pairs( event.params.buttons ) do
		local button = widget.newButton( {
			x = consts.centerX,
			y = top,
			width = consts.width,
			height = consts.mmBtnHeight,
			label = button.label,
			font = font.normal,
			fontSize = 28,
			defaultFile = "game_menu_btn.png",
			overFile = "game_menu_btn_pressed.png",
			onRelease = button.listener
		} )
		menu:insert( button )

		top = top + 120
	end
end

scene:addEventListener( "createScene", scene )

return scene