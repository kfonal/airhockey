local widget = require "widget"
local preferences = require "preferences"

function newSlideView(imageSet, left, top, width, height, itemWidth, itemHeight, preferenceOptions, preferenceCurrent, preferenceKey)
	local scrollView = {}
	
	function scrollListener(event)
		local x,y = scrollView:getContentPosition()
		
		if (event.phase == "moved") then
			if (x >= -#imageSet*itemWidth*0.5 + width/2 and event.direction == "right") then
				print("replace left set of images with middle")
				scrollView:scrollToPosition{ 
					x = -#imageSet*itemWidth*1.5 + width/2 ,
					y = 0,
					time = 0
				}
			elseif (x <= -#imageSet*itemWidth*2.5 + width/2 and event.direction == "left") then				
				print("replace right set of images with middle")
				scrollView:scrollToPosition{ 
					x = -#imageSet*itemWidth*1.5 + width/2 ,
					y = 0,
					time = 0
				}
			end
		elseif (event.phase == "ended") then
			local selected = (x - width/2 + itemWidth/2) / itemWidth
			scrollView.selected = math.round(selected)*(-1) + 1
			print("selected: "..scrollView.selected) 
			--scroll to center of selected item
			scrollView:scrollToSelected(selected)
			--save selected option
			local prefValue = scrollView.selected % #preferenceOptions
			if (prefValue == 0) then prefValue = #preferenceOptions end
			preferences[preferenceKey] = preferenceOptions[prefValue]
			print("Saved: "..preferenceKey.."="..preferenceOptions[prefValue])
			preferences:save()
		end
	end
	
	scrollView = widget.newScrollView
	{
		left = left,
		top = top,
		width = width,
		height = height,
		horizontalScrollDisabled = false,
		verticalScrollDisabled = true,
		hideScrollBar = true,
		listener = scrollListener,
		hideBackground = true
	}

	--selected item - current choosen option + length of options - to be in middle of images
	local selected = table.indexOf(preferenceOptions, preferenceCurrent)
	if (selected == nil) then selected = 1 end
	scrollView.selected = selected + #preferenceOptions

	function scrollView:scrollToSelected()
		self:scrollToPosition{ 
			x = -(self.selected-1)*itemWidth - itemWidth/2 + width/2 ,
			y = 0,
			time = 200
		}
	end
	
	for j=1,3 do
		for i=1,#imageSet do
			local img = display.newImageRect(imageSet[j], itemWidth, itemHeight)
			img.anchorX = 0
			img.anchorY = 0
			img.x = (j-1) * itemWidth + (i-1) * #imageSet * itemWidth
			img.y = 0
			scrollView:insert(img)
		end
	end
	
	scrollView:scrollToSelected()
	
	return scrollView
end

