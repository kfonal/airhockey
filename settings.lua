local consts = require "consts"
local storyboard = require "storyboard"
local slideview = require "slideview"
local preferences = require "preferences"

local scene = storyboard.newScene()

function scene:createScene(event)
	local group = self.view

	local bg = display.newImageRect(group, "bg.png", consts.width, consts.height);
	bg.x = consts.centerX
	bg.y = consts.centerY
	
	--goals to win settings
	local gtowinLabel = display.newImageRect(group, "settings_gtowin.png", consts.width, 50)
	gtowinLabel.x = consts.centerX
	gtowinLabel.y = 25
	
	local gtowinSlideView = newSlideView({
			"five.png",
			"seven.png",
			"nine.png"
		}, 0, 50, consts.width, 170, 280, 170,
		{
			5, 
			7,
			9
		}, preferences["goalsToWin"], "goalsToWin")
	group:insert(gtowinSlideView)
	
	--cpu level settings
	local cpulevelLabel = display.newImageRect(group, "settings_cpulevel.png", consts.width, 50)
	cpulevelLabel.x = consts.centerX
	cpulevelLabel.y = 255
	
	local cpulevelSlideView = newSlideView({
			"level1.png",
			"level2.png",
			"level3.png"
		}, 0, 280, consts.width, 170, 280, 170,
		{
			"easy",
			"medium",
			"hard"
		}, preferences["cpuLevel"], "cpuLevel")
	group:insert(cpulevelSlideView)
	
	--theme settings
	local themeLabel = display.newImageRect(group, "settings_theme.png", consts.width, 50)
	themeLabel.x = consts.centerX
	themeLabel.y = 485
	
	local themeSlideView = newSlideView({
			"theme1.png",
			"theme2.png",
			"theme3.png"
		}, 0, 510, consts.width, 170, 280, 170, 
		{
			"ice",
			"wood",
			"plastic"
		}, preferences["theme"], "theme")
	group:insert(themeSlideView)
end

function scene:enterScene(event)
	storyboard.onBackEvent = nil
	storyboard.backTo = "mainmenu"
end

scene:addEventListener( "createScene", scene )
scene:addEventListener( "enterScene", scene )

preferences:save()

return scene