local storyboard = require "storyboard"
local multiplayer = require "multiplayer"
local widget = require "widget"

local scene = storyboard.newScene()

function scene:createScene(event)
	local group = self.view
	
	local bg = display.newImageRect("bg.png", consts.width, consts.height)
	bg.x = consts.centerX
	bg.y = consts.centerY
	group:insert(bg)
	
	local invitePlayerBtn = widget.newButton {
		left = 0,
		top = 90,
		width = consts.width,
		height = consts.mmBtnHeight,
		defaultFile = "menu_invite_player.png",
		--overFile = "menu_1player_selected.png",
		onRelease = function() multiplayer.selectPlayer() end 
	}
	group:insert(invitePlayerBtn)
	
	local invitationsBtn = widget.newButton {
		left = 0,
		top = 210,
		width = consts.width,
		height = consts.mmBtnHeight,
		defaultFile = "menu_invitations.png",
		--overFile = "menu_2players_selected.png",
		onRelease = function() multiplayer.invitations() end
	}
	group:insert(invitationsBtn)
end

function scene:enterScene(event)
	storyboard.backTo = "mainmenu"
end

scene:addEventListener( "createScene", scene )
scene:addEventListener( "enterScene", scene )

return scene