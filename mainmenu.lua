local widget = require "widget"
local storyboard = require "storyboard"
local consts = require "consts"
local cb = require "chartboostSDK.chartboost"
local font = require "font"
local ads = require "ads"
local social = require "social"
local multiplayer = require "multiplayer"

local scene = storyboard.newScene()

--local socialLoginLogoutBtn, singlePlayerBtn, oneOnOneBtn, multiplayerBtn, achievementsBtn, settingsBtn 

local function onSettingsBtnClick(event)
	storyboard.gotoScene( "settings", { effect = "fromTop", time = 300 } )
end

local function onSinglePlayerBtnClick(event)
	storyboard.gotoScene( "gamescene", { effect = "fromTop", time = 300, params = { mode = "single" } } )
end

local function onOneOnOneBtnClick(event)
	storyboard.gotoScene( "gamescene", { effect = "fromTop", time = 300, params = { mode = "oneonone" } } )
end

local function onAchievementsBtnClick(event)
	if (social.isConnected()) then
		social.showAchievements()
	else
		social.login( function () social.showAchievements() end )
	end
end

local function onMultiplayerBtnClick(event)
	storyboard.gotoScene( "multiplayermenu", { effect = "fromTop", time = 300 } )
end

function scene:createScene(event)
	local group = self.view
	
	local bg = display.newImageRect("bg.png", consts.width, consts.height)
	bg.x = consts.centerX
	bg.y = consts.centerY
	group:insert(bg)
	
	local singlePlayerBtn = widget.newButton {
		left = 0,
		top = 90,
		width = consts.width,
		height = consts.mmBtnHeight,
		defaultFile = "menu_1player.png",
		overFile = "menu_1player_selected.png",
		onRelease = onSinglePlayerBtnClick
	}
	group:insert(singlePlayerBtn)
	
	local oneOnOneBtn = widget.newButton {
		left = 0,
		top = 210,
		width = consts.width,
		height = consts.mmBtnHeight,
		defaultFile = "menu_2players.png",
		overFile = "menu_2players_selected.png",
		onRelease = onOneOnOneBtnClick
	}
	group:insert(oneOnOneBtn)
	
	local multiplayerBtn = widget.newButton {
		left = 0,
		top = 330,
		width = consts.width,
		height = consts.mmBtnHeight,
		defaultFile = "menu_multiplayer.png",
		overFile = "menu_multiplayer_selected.png",
		onRelease = onMultiplayerBtnClick
	}
	group:insert(multiplayerBtn)
	
	local achievementsBtn = widget.newButton {
		left = 0,
		top = 450,
		width = consts.width,
		height = consts.mmBtnHeight,
		defaultFile = "menu_achievements.png",
		overFile = "menu_achievements_selected.png",
		onRelease = onAchievementsBtnClick
	}
	group:insert(achievementsBtn)
	
	local settingsBtn = widget.newButton {
		left = 0,
		top = 570,
		width = consts.width,
		height = consts.mmBtnHeight,
		defaultFile = "menu_settings.png",
		overFile = "menu_settings_selected.png",
		onRelease = onSettingsBtnClick
	}
	group:insert(settingsBtn)

	print("Created mainmenu scene")
end

local function adListener( event )
    if event.isError then
        -- Failed to receive an ad.
        print("failed to receive ad")
        print(event.response)
    end
end

function scene:enterScene()
	storyboard.onBackEvent = nil
	storyboard.backTo = nil

	if (not ads.isVisible) then
		ads:setCurrentProvider( "admob" )
		ads.show( "banner", { x=0, y=display.contentHeight - display.screenOriginY - (display.contentWidth / 320 * 50) } )
		ads.isVisible = true
	end
	cb.showInterstitial()
end

scene:addEventListener( "createScene", scene )
scene:addEventListener( "enterScene", scene )

return scene 