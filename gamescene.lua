local storyboard = require "storyboard"
local font = require "font"
local consts = require "consts"
local preferences = require "preferences"
local gameObject = require "gameobject"
local ads = require "ads"
local social = require "social"
local str = require "str"
local multiplayer = require "multiplayer"

local scene = storyboard.newScene()

--game objects
local players, puck, table, result

--game mode like: single, oneonone, multiplayer
local mode = nil

--keeping last enter frame time to count elapsed time between frames
local lastEnterFrameTime = nil

--playes start positions
local player1StartPosition, player2StartPosition = 700, 100

--basic operations on game objects
local function updateResult()
	result.text = players["player1"].scores.." : "..players["player2"].scores
end

local function resetGame()
	players["player1"].scores = 0
	players["player2"].scores = 0
	players["player1"]:setPosition(consts.centerX, player1StartPosition)
	players["player2"]:setPosition(consts.centerX, player2StartPosition)
	puck:setPosition(consts.centerX, consts.centerY)
	puck:resetForces()

	updateResult()
end

--buttons for in-game menu and their actions
local mainMenuBtn = { label = "Main Menu", listener = 
	function () 
		if (mode == "multiplayer") then
			multiplayer.leaveRoom()
		end
		storyboard.gotoScene( "mainmenu", { effect = "fromTop", time = 300 } )
		storyboard.removeScene( "gamescene" )
	end 
}

local rematchBtn = { label = "Rematch", listener = 
	function () 
		resetGame(); storyboard.hideOverlay( "zoomOutInFade", 300 ) 
	end 
}

local resumeBtn = { label = "Resume", listener = 
	function()
		storyboard.hideOverlay( "zoomOutInFade", 300 )
	end
}

local function unlockAchievement(player)
	if (mode == "single" and player == "player1") then
		if (preferences.cpuLevel == "easy") then
			social.unlockAchievement(social.achievements.easyCpuSlayer)
		elseif (preferences.cpuLevel == "medium") then
			social.unlockAchievement(social.achievements.mediumCpuSlayer)
		elseif (preferences.cpuLevel == "hard") then
			social.unlockAchievement(social.achievements.hardCpuSlayer)
		end
	end
end

local function showMenu(buttons, backBlocked)
	if (backBlocked) then
		storyboard.overlayBlocked = true
	else
		storyboard.overlayBlocked = true
	end

	storyboard.showOverlay( "gamemenu", { isModal = true, effect = "zoomOutIn", time = 300, 
		params = { buttons = buttons } } )
end

local function showGoalAnimation(playerName)
	local goalText = display.newText( 
		{ text = "Goal for "..playerName, x = consts.centerX, y = consts.centerY, font = font.normal, fontSize = 22 } )
	goalText:setFillColor(preferences:getThemeTextColor())
	
	transition.scaleTo(goalText, {xScale = 2.0, yScale = 2.0, time = 2500 } )
	transition.to( goalText, { delay = 1500, time = 1500, alpha = 0.0, onComplete = function() goalText:removeSelf() end } )
end

local function showWinAnimation(playerName, listener)
	local winText = display.newText( 
		{ text = playerName.." won!", x = consts.centerX, y = consts.centerY, font = font.normal, fontSize = 22 } )
	winText:setFillColor(preferences:getThemeTextColor())

	transition.blink(winText, { time = 2500 } )
	--after win animation is finished call listener for ending game
	transition.scaleTo( winText, { xScale = 2.0, yScale = 2.0, time = 2500, onComplete = function () winText:removeSelf(); listener() end } )
end

local function onGoalEvent(event)
	print("goal for "..event.goalFor)
	
	--pause game, hide puck and increase scores
	physics.pause()
	players[event.goalFor].scores = players[event.goalFor].scores + 1
	puck:hide()

	--reset puck and players positions
	timer.performWithDelay( 2500, 
		function () 
			puck:resetForces() 
			players["player1"]:setPosition(consts.centerX, player1StartPosition)
			players["player2"]:setPosition(consts.centerX, player2StartPosition)
			if (players["player2"].cpuStrategy) then
				players["player2"].cpuStrategy.timeOfLastAttack = -0.5
			end
			tempY = select(2, players[event.goalFor]:getPosition())
			puck:setPosition(consts.centerX, consts.centerY + (consts.centerY - tempY) / 2)  
			puck:show()
			physics.start()
		end )
	
	--change result text
	updateResult()

	--show goal or win text animation (if win unlock achievement also and show proper menu depending on game mode)
	if (players[event.goalFor].scores == tonumber(preferences["goalsToWin"])) then
		showWinAnimation(players[event.goalFor].name, 
			function()
				showMenu( { mainMenuBtn, rematchBtn }, true ) 
				unlockAchievement(event.goalFor)
			end
		)
	else
		showGoalAnimation(players[event.goalFor].name)
	end
end

local function onShakeEvent(event)
	if (event.isShake) then
		showMenu( { mainMenuBtn, rematchBtn, resumeBtn }, false )
	end
end

local function onMessageReceived(message)
    if (message.type == "move") then
        local location = str.split(message.content, ";")
        local x,y = location[1], location[2]
        players["player2"].body.touchJoint:setTarget( x, y)
    end
end

local function onEnterFrame(event)
	if (lastEnterFrameTime) then
		--if player1 is cpu then run his move strategy
		if (players["player2"].mode == "cpu") then
			players["player2"].cpuStrategy:doMove(players["player2"], puck, (event.time - lastEnterFrameTime) / 1000)
		end
	end

	lastEnterFrameTime = event.time
end

function scene:createScene(event)
	local mainView = self.view

	physics.start( )
	physics.setGravity( 0, 0 )
	--physics.setDrawMode( "debug" ) 

	--set mode
	mode = event.params.mode

	--create game objects
	table = gameObject:newTable(mainView, "table-"..preferences["theme"]..".png")
	puck = gameObject:newPuck(mainView, "puck-"..preferences["theme"]..".png", consts.centerX, consts.centerY)
	players = {}
	players["player1"] = gameObject:newPlayer(mainView, 
		"player 1", 
		"player1-"..preferences["theme"]..".png", 
		consts.centerX, player1StartPosition)
	players["player1"].mode = "touchable"
	--player 2 depends on mode
	if (mode == "single") then 
		players["player2"] = gameObject:newPlayer(mainView, 
			"cpu player", 
			"player2-"..preferences["theme"]..".png", 
			consts.centerX, player2StartPosition, 
			preferences["cpuLevel"])
		players["player2"].mode = "cpu"
	else
		players["player2"] = gameObject:newPlayer(mainView, 
			"player 2", 
			"player2-"..preferences["theme"]..".png", 
			consts.centerX, player2StartPosition)
		players["player2"].mode = "touchable"
	end

	--set players mode accoridng to game mode and listener for incoming events
	if (mode == "multiplayer") then
		players["player1"].mode = "multiplayer"
		players["player2"].mode = "multiplayer"
	end

	--create additional objects
	result = display.newText( { parent = mainView, text = "0 : 0", x = 35, y = consts.centerY, font = font.normal, fontSize = 20 } )
	result:setFillColor(preferences:getThemeTextColor())
	result:rotate(-90)
end

function scene:enterScene(event)
	--set goal event to know when goal was shot
	puck.onGoal = onGoalEvent

	--set messsage listener for multiplayer
	multiplayer.onMessageReceived = onMessageReceived

	storyboard.onBackEvent = 
		function() 
			if ( not storyboard.isOverlay ) then 
				showMenu( { mainMenuBtn, rematchBtn, resumeBtn }, false )
			end
		end
	storyboard.backTo = nil

	ads:setCurrentProvider( "admob" )
	ads.hide( )
	ads.isVisible = false
end

function scene:exitScene( event )
    Runtime:removeEventListener( "accelerometer", onShakeEvent )
    Runtime:removeEventListener( "enterFrame", onEnterFrame )
    players["player1"] = nil
    players["player2"] = nil
    players = nil
    puck = nil
    table = nil 
    result = nil
end

function scene:overlayBegan( event )
	storyboard.isOverlay = true
    physics.pause( )
end

function scene:overlayEnded( event )
	storyboard.isOverlay = false
    physics.start( )
end

scene:addEventListener( "createScene", scene )
scene:addEventListener( "enterScene", scene )
scene:addEventListener( "exitScene", scene )
scene:addEventListener( "overlayBegan" )
scene:addEventListener( "overlayEnded" )

Runtime:addEventListener( "accelerometer", onShakeEvent )
Runtime:addEventListener( "enterFrame", onEnterFrame )

return scene