local consts = require "consts"

local lastStrategy = ""

local function getDistance(x1, y1, x2, y2)
	return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1-y2, 2))
end

--[[Medium strategy is base strategy for rest, in rest only params and getStrategy function change--]]
local mediumStrategy = { 
	slowPace = 0.12,
	normalPace = 0.18,
	fastPace = 0.32,
	puckFastPace = 900,
	timeOfLastAttack = 1.0,
	defensiveArea = { left = consts.centerX - 120, right = consts.centerX + 120, up = consts.centerY - 300 },
	passiveArea = { left = consts.centerX - 160, right = consts.centerX + 160, up = consts.centerY - 230 }
}
mediumStrategy.__index = mediumStrategy

function mediumStrategy:doPassive(selfX, selfY, puckX, puckY)
	if (lastStrategy ~= "passive") then
		--print("passive")
		lastStrategy = "passive"
	end

	local x, y = (((self.passiveArea.right - self.passiveArea.left) * puckX) / consts.tableWidth + self.passiveArea.left) - selfX, self.passiveArea.up - selfY
	return x,y,self.slowPace
end

function mediumStrategy:doDefence(selfX, selfY, puckX, puckY)
	if (lastStrategy ~= "defence") then
		--print("defence")
		lastStrategy = "defence"
	end

	local x, y = (((self.defensiveArea.right - self.defensiveArea.left) * puckX) / consts.tableWidth + self.defensiveArea.left) - selfX, self.defensiveArea.up - selfY
	return x,y,self.normalPace
end

function mediumStrategy:doAttack(selfX, selfY, puckX, puckY)
	if (lastStrategy ~= "attack") then
		--print("attack")
		lastStrategy = "attack"
	end

	if (self.timeOfLastAttack > 0.7) then
		local x,y = puckX - selfX, puckY -	selfY - consts.puckR / 10

		--mark that attack was done if collision detected
		if (getDistance(selfX, selfY, puckX, puckY) <= (consts.puckR + consts.playerR)) then 
			self.timeOfLastAttack = 0.0 
		end

		return x,y,self.fastPace
	else
		--back from attack - player should go from puck
		local verticalDist = consts.puckR * 4
		local horizontalDist = consts.puckR + consts.playerR + 20

		local x = puckX - selfX + ((puckX < consts.tableWidth / 2) and horizontalDist or -horizontalDist) 
		local y = puckY - selfY - verticalDist

		return x,y,self.fastPace
	end
end

function mediumStrategy:getRealDestPosition(selfX, selfY, destX, destY, pace)
	--now destination must be recalculated taking a speed into consideration
	local x, y = (destX * pace) + selfX, (destY * pace) + selfY

	return x,y;
end

function mediumStrategy:getStrategy(isPuckPaceFast, isPuckComingInCpuDir, isPuckOnCpuHalf)
	--print(tostring(isPuckPaceFast).." "..tostring(isPuckComingInCpuDir).." "..tostring(isPuckOnCpuHalf))
	if (not isPuckPaceFast and not isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doPassive end
	if (not isPuckPaceFast and not isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doAttack end
	if (not isPuckPaceFast and isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (not isPuckPaceFast and isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doAttack end
	if (isPuckPaceFast and not isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doPassive end
	if (isPuckPaceFast and not isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doPassive end
	if (isPuckPaceFast and isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (isPuckPaceFast and isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doDefence end
end

function mediumStrategy:doMove(player, puck, delta)
	self.timeOfLastAttack = self.timeOfLastAttack + delta
	
	local puckVX, puckVY = puck.body:getLinearVelocity()
	local puckX, puckY = puck:getPosition()
	local selfX, selfY = player:getPosition()
	--if puck is not moving and on the opposite side do not any move
	--print("puck pace: "..getDistance(0,puckVX,0,puckVY))	
	if (puckVX ~= 0 or puckVY ~= 0 or puckY <= consts.centerY) then
		local strategyFunction = self:getStrategy(getDistance(0,puckVX,0,puckVY) > self.puckFastPace, puckVY < 0, puckY <= consts.centerY)
		local targetX, targetY = self:getRealDestPosition(selfX, selfY, strategyFunction(self, selfX, selfY, puckX, puckY))
		player.body.touchJoint:setTarget(targetX, targetY)
	end
end

--[[Easy strategy--]]
local easyStrategy = {
	slowPace = 0.06,
	normalPace = 0.1,
	fastPace = 0.22,
	puckFastPace = 800,
	defensiveArea = { left = consts.centerX - 140, right = consts.centerX + 140, up = consts.centerY - 280 },
	passiveArea = { left = consts.centerX - 180, right = consts.centerX + 180, up = consts.centerY - 210 }
}
setmetatable( easyStrategy, mediumStrategy )
--easyStrategy.__index = mediumStrategy

function easyStrategy:getStrategy(isPuckPaceFast, isPuckComingInCpuDir, isPuckOnCpuHalf)
	--print(tostring(isPuckPaceFast).." "..tostring(isPuckComingInCpuDir).." "..tostring(isPuckOnCpuHalf))
	if (not isPuckPaceFast and not isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doPassive end
	if (not isPuckPaceFast and not isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doAttack end
	if (not isPuckPaceFast and isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (not isPuckPaceFast and isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doAttack end
	if (isPuckPaceFast and not isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doPassive end
	if (isPuckPaceFast and not isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doPassive end
	if (isPuckPaceFast and isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (isPuckPaceFast and isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doDefence end
end

--[[Hard strategy--]]
local hardStrategy = {
	slowPace = 0.17,
	normalPace = 0.26,
	fastPace = 0.48,
	puckFastPace = 1200,
	defensiveArea = { left = consts.centerX - 100, right = consts.centerX + 100, up = consts.centerY - 310 },
	passiveArea = { left = consts.centerX - 140, right = consts.centerX + 140, up = consts.centerY - 240 }
}
setmetatable( hardStrategy, mediumStrategy )
hardStrategy.__index = mediumStrategy

function hardStrategy:getStrategy(isPuckPaceFast, isPuckComingInCpuDir, isPuckOnCpuHalf)
	--print(tostring(isPuckPaceFast).." "..tostring(isPuckComingInCpuDir).." "..tostring(isPuckOnCpuHalf))
	if (not isPuckPaceFast and not isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doPassive end
	if (not isPuckPaceFast and not isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doAttack end
	if (not isPuckPaceFast and isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (not isPuckPaceFast and isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doAttack end
	if (isPuckPaceFast and not isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (isPuckPaceFast and not isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (isPuckPaceFast and isPuckComingInCpuDir and not isPuckOnCpuHalf) then return mediumStrategy.doDefence end
	if (isPuckPaceFast and isPuckComingInCpuDir and isPuckOnCpuHalf) then return mediumStrategy.doDefence end
end

local strategies = { easy = easyStrategy, medium = mediumStrategy, hard = hardStrategy }

return strategies

